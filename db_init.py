import os

if not os.path.exists('db.sqlite3'):
    from app import db, create_app
    from app import User
    db.create_all(app=create_app())

app = create_app()
alex = User(username='Alex')
with app.app_context():
    db.session.add(alex)
    db.session.commit()
