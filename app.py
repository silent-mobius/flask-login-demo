import os
import sys
from flask import Flask
from flask_login import LoginManager, UserMixin 
from flask_login import login_required,login_user,current_user
from flask_sqlalchemy import SQLAlchemy



login_manager = LoginManager()
db = SQLAlchemy()

class User(UserMixin,db.Model):
    id = db.Column(db.Integer,primary_key=True)
    username = db.Column(db.String(50), unique=True)



def create_app():
    
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'You?ll_NeveR+gUE55!!'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
    app.config['SQLALCHEMY_TRACK_MODIFICATION'] = False


    login_manager.init_app(app)
    db.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))

    @app.route('/profile')
    @login_required
    def profile():
        return 'logged in the profile'

    @app.route('/login')
    def login():
        user = User.query.filter_by(username='Alex').first()
        login_user(user)
        return 'logging to profile'

    return app